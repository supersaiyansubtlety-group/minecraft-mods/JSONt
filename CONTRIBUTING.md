# Contribution Guidelines

Please feel free to contrubute by opening an issue or creating a merge request.

## Entry Template

```markdown
##### MOD NAME

MOD DESCRIPTION (preferably a quote from the project, if an approproiate quote exists)

Links: 
[Tutorial](),
[Wiki](),
[Source](),
[Modrinth](),
[CurseForge]()
```

Please omit missing links.

If both Wiki and Tutorial links are missing, consider adding an `[Example]()` link.
