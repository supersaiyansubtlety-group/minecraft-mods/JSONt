# JSON't
A list of modern Fabric/Quilt Minecraft mods that reduce the amount of JSON mod authors need to write by hand.

'Modern' means they run on at least one of two most recent full releases of Minecraft.

If you have questions or ideas about this list, see [CONTRIBUTING](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/JSONt/-/blob/master/CONTRIBUTING.md).

## Dynamic
These run at runtime and either create JSON objects or directly create objects without involving JSON serializers (like directly creating a recipe/loot table without any serialization).

##### ARRP

> Advanced Runtime resource packs (for fabric)

Links: 
[Wiki](https://github.com/Devan-Kerman/ARRP/wiki),
[Source](https://github.com/Devan-Kerman/ARRP)

##### Artifice

> A library mod for Minecraft 1.15+, for programmatically generated resource files.

Links: 
[Example (testmod)](https://github.com/vampire-studios/artifice/blob/1.18/src/testmod/java/com/swordglowsblue/artifice/test/ArtificeTestMod.java),
[Source (current, maintained)](https://github.com/vampire-studios/artifice),
[Source (archived, 1.17-)](https://github.com/natanfudge/artifice)

##### GSON (DIY)
Use the GSON library to create `JsonObject`s

Links: 
[Tutorial (Recipes)](https://fabricmc.net/wiki/tutorial:dynamic_recipe_generation),
[Tutorial (Models)](https://fabricmc.net/wiki/tutorial:dynamic_model_generation),
[Wiki](https://sites.google.com/site/gson/gson-user-guide),
[Source](https://github.com/google/gson)

## Static
These generate JSON only in develeopment environments, and not at runtime.

##### fabric-data-generation-api-v1

> This API extends the data generation system used by Minecraft to allow for mods to generate data (json files) automatically

Links: 
[Example (testmod)](https://github.com/FabricMC/fabric/blob/1.18/fabric-data-generation-api-v1/src/testmod/java/net/fabricmc/fabric/test/datagen/DataGeneratorTestEntrypoint.java),
[Source](https://github.com/FabricMC/fabric/tree/1.18/fabric-data-generation-api-v1)

## Other
These reduce the amount of JSON that must be written in some other way.

### Mods
##### Defaulted Drops

> This is a tiny utility mod for mod makers. If a mod opts in (via fabric.mod.json), blocks from that mod that don't have loot tables will drop the item form of that block.

Links: 
[Wiki](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/defaulted_drops/-/wikis/home),
[Source](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/defaulted_drops/),
[Modrinth](https://modrinth.com/mod/defaulted-drops),
[CurseForge](https://www.curseforge.com/minecraft/mc-mods/defaulted-drops)

##### Stonecutter Recipe Tags

> This Fabric mod allows for creation of stonecutter recipes through tags. Any item in these tags can be crafted into any other item in the same tag. This allows for large decreases in JSON files and recipe counts, cutting file counts and reducing lag.

Links: 
[Wiki](https://github.com/TropheusJ/stonecutter-recipe-tags#stonecutter-recipe-tags),
[Source](https://github.com/TropheusJ/stonecutter-recipe-tags),
[Modrinth](https://modrinth.com/mod/stonecutter_recipe_tags),
[CurseForge](https://www.curseforge.com/minecraft/mc-mods/stonecutter-recipe-tags)

### External Tools

##### JSON-Generator
> Easy-to-use generator to make the JSON files primarily for mods, but can also be used for resource packs and datapacks. Comes with extensive generation tools to maximize its usefulness.

Links: [Source](https://github.com/Deviouslrd/json-generator/tree/tauri)
